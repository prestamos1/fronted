import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import clienteAxios from '../../config/axios';
import MonedaComponent from '../../config/moneda';

const ListCreditosComponent = () => {

    const [creditos, setCreditos] = useState([])

    const getCreditos = async () => {
        const res = await clienteAxios.get('/creditos')
        console.log(res.data)
        setCreditos(res.data)
    }

    useEffect(() => {
        getCreditos()
    }, [])
    

    return ( 
        
        <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
            <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                    Creditos
                </div>
                <div className="p-3">
                    <form className="w-full float-root" >
                        <Link to={"/credito/create"} className="bg-green-500 hover:bg-green-800 text-white py-2 px-4 rounded float-right mb-5"> <i className='fa fa-plus-circle'></i> Nuevo Credito</Link>
                        <table className="table text-grey-darkest">
                            <thead className="bg-grey-dark text-white text-normal">
                                <tr>
                                    <th scope="col">Factura</th>
                                    <th scope="col">Cliente</th>
                                    <th scope="col">Monto</th>
                                    <th scope="col">Pagadas</th>
                                    <th scope="col">Pendientes</th>
                                    <th scope="col">En Mora</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {creditos.map((credito) => (
                                    <tr key={credito.id}>
                                        <td className='underline text-blue-500 text-center'><Link to={`/factura/${credito.factura.id}`}>{credito.factura.numero_documento}</Link></td>
                                        <td className='underline text-blue-500 text-center'><Link to={`/cliente/${credito.cliente.id}`}>{credito.cliente.nombres} {credito.cliente.apellidos}</Link></td>
                                        <td className='text-center'> <MonedaComponent /> {credito.monto.toFixed(2)}</td>
                                        <td className='text-center'>{credito.pagadas_count}</td>
                                        <td className='text-center'>{credito.sinpagar_count}</td>
                                        <td className='text-center'>{credito.mora_count}</td>
                                        <td>
                                            <Link to={`/credito/${credito.id}`} className="bg-info-light border border-info-600 cursor-pointer rounded p-1 mx-1 text-green-500">
                                                <i className="fas fa-eye"></i></Link>
                                        </td>
                                    </tr>
                                ) )}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
     );
}
 
export default ListCreditosComponent;