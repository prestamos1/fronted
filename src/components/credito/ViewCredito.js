import React, { Fragment, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import clienteAxios from '../../config/axios';
import MonedaComponent from '../../config/moneda';


const ViewCreditoComponent = () => {
    const [credito, setCredito] = useState('')
    const [cuotas, setCuotas] = useState([])
    const [cliente, setCliente] = useState('')

    const { id } = useParams()

    const getData = async () => {
        const res = await clienteAxios.get('/credito/detalle/'+id)

        console.log(res.data.cuotas)

        setCliente(res.data.cliente)
        setCuotas(res.data.cuotas)
        setCredito(res.data.credito)
    }

    useEffect(() => {
        getData()
    }, [])
    return ( 
        <Fragment>
            <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                    <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                        Informacion Cliente
                    </div>
                    <div className="p-3">
                        <form className="w-full float-root">
                            <div className="flex flex-wrap -mx-3 mb-6">

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        DOCUMENTO
                                    </label>
                                    <input
                                        disabled
                                        value={cliente.documento}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>
                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Nombres
                                    </label>
                                    <input
                                        disabled
                                        value={cliente.nombres}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-first-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Apellidos
                                    </label>
                                    <input
                                        disabled
                                        value={cliente.apellidos}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-first-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Telefono
                                    </label>
                                    <input
                                        disabled
                                        value={cliente.telefono} className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-first-name" type="text" placeholder="" />
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                    <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                        Informacion del Credito
                    </div>
                    <div className="p-3">
                        <form className="w-full float-root" >
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Monto Total
                                    </label>
                                    <input
                                        disabled
                                        value={credito.monto}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Inicial
                                    </label>
                                    <input
                                        disabled
                                        value={credito.inicial}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Monto a acreditar
                                    </label>
                                    <input
                                        disabled
                                        value={credito.credito}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Interes (%)
                                    </label>
                                    <input
                                        disabled
                                        value={credito.interes}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                            </div>
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Numero de Cuotas
                                    </label>
                                    <input
                                        disabled
                                        value={credito.coutas}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Intervalos de Cuota
                                    </label>
                                    <select
                                        disabled
                                        value={credito.intervalo}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    >
                                        <option value="0">SELECCIONE</option>
                                        <option value="1">DIARIO</option>
                                        <option value="7">SEMANAL</option>
                                        <option value="15">QUINCENAL</option>
                                        <option value="30">MENSUAL</option>
                                    </select>
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        FECHA PRIMERA COUTA
                                    </label>
                                    <input
                                        disabled
                                        value={credito.primera_cuota}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="date" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        FECHA ULTIMA COUTA
                                    </label>
                                    <input
                                        disabled
                                        value={credito.ultima_cuota}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="date" placeholder="" />
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                    <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                        Informacion del Credito
                    </div>
                    <div className="p-3">
                        <form className="w-full float-root" >
                                        <table className="hidden w-full text-left table-auto lg:table">
                                            <thead>
                                                <tr>
                                                    <th className="py-2 pr-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Pagar</th>
                                                    <th className="py-2 pr-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Fecha</th>
                                                    <th className="px-2 py-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Monto</th>
                                                    <th className="px-2 py-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Interes</th>
                                                    <th className="py-2 pl-2 text-xs font-medium tracking-wider text-right text-gray-500 uppercase leading-4">Total</th>
                                                </tr>
                                            </thead>
                                <tbody>
                                                {cuotas.map((cuota) => (
                                                    <tr key={cuota.id}>
                                                        <td className="py-2 pr-2 whitespace-nowrap pb-6">
                                                            {(() => {
                                                                if (cuota.estadoCuota === 1) {
                                                                    return <Link to={`/cuota/pagar/${cuota.id}`} className="shadow bg-yellow-500 hover:bg-yellow-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
                                                                    >
                                                                        Pagar
                                                                    </Link>
                                                                }

                                                                else if (cuota.estadoCuota === 2) {
                                                                    return <span class="bg-green-500 text-white text-xs font-semibold mr-2 px-4 pt-2 pb-2 py-0.5 rounded dark:bg-green-200 dark:green-red-900">Pagado</span>
                                                                }

                                                                else if (cuota.estadoCuota === 3) {
                                                                    return <Fragment><Link to={`/cuota/pagar/${cuota.id}`} className="shadow bg-red-500 hover:bg-red-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
                                                                        t>
                                                                        Pagar
                                                                    </Link>
                                                                        <i className="fas fa-exclamation-circle text-red-900"></i>
                                                                    </Fragment>
                                                                }
                                                            })()}

                                                        </td>
                                                        <td className="py-2 pr-2 font-bold whitespace-nowrap"> {cuota.fecha_couta}</td>
                                                        <td className="px-2 py-2 whitespace-nowrap"><MonedaComponent /> {cuota.monto}</td>
                                                        <td className="px-2 py-2 whitespace-nowrap"><MonedaComponent /> {cuota.interes}</td>
                                                        <td className="py-2 pl-2 text-right whitespace-nowrap"><MonedaComponent /> {cuota.total}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
     );
}
 
export default ViewCreditoComponent;