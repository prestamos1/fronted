import React, { Fragment, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import clienteAxios from '../../config/axios';

const ViewCuotaComponent = () => {


    const [cuota, setCuota] = useState('')
    const [credito, setCredito] = useState('')
    const [cliente, setCliente] = useState('')
    const { id } = useParams()
    const navigate = useNavigate()

    const getData = async () => {
        const res = await clienteAxios.get('/cuota/detalle/' + id)
        setCuota(res.data.cuota)
        setCliente(res.data.cliente)
        setCredito(res.data.credito)
    }

    const updateCuota = async (e) => {
        e.preventDefault();
        const res = await clienteAxios.post('/cuota/update', {
            idCuota: cuota.id,
        })

        if (res.data.status === true) {
            navigate('/cliente/'+cliente.id)
        }
    }


    useEffect(() => {
        getData()
    }, [])
    return ( 
        <Fragment>
            <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                    <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                        Informacion Cliente
                    </div>
                    <div className="p-3">
                        <form className="w-full float-root">
                            <div className="flex flex-wrap -mx-3 mb-6">
                                
                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        DOCUMENTO
                                    </label>
                                    <input
                                        disabled
                                        value={cliente.documento}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>
                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Nombres
                                    </label>
                                    <input
                                        disabled
                                        value={cliente.nombres}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-first-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Apellidos
                                    </label>
                                    <input
                                        disabled
                                        value={cliente.apellidos}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-first-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Telefono
                                    </label>
                                    <input
                                        disabled
                                        value={cliente.telefono} className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-first-name" type="text" placeholder="" />
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                    <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                        Informacion del Credito
                    </div>
                    <div className="p-3">
                        <form className="w-full float-root" >
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Monto Total
                                    </label>
                                    <input
                                        disabled
                                        value={credito.monto}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Inicial
                                    </label>
                                    <input
                                        disabled
                                        value={credito.inicial}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Monto a acreditar
                                    </label>
                                    <input
                                        disabled
                                        value={credito.credito}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Interes (%)
                                    </label>
                                    <input
                                        disabled
                                        value={credito.interes}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                            </div>
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Numero de Cuotas
                                    </label>
                                    <input
                                        disabled
                                        value={credito.coutas}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Intervalos de Cuota
                                    </label>
                                    <select
                                        disabled
                                        value={credito.intervalo}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    >
                                        <option value="0">SELECCIONE</option>
                                        <option value="1">DIARIO</option>
                                        <option value="7">SEMANAL</option>
                                        <option value="15">QUINCENAL</option>
                                        <option value="30">MENSUAL</option>
                                    </select>
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        FECHA PRIMERA COUTA
                                    </label>
                                    <input
                                        disabled
                                        value={credito.primera_cuota}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="date" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        FECHA ULTIMA COUTA
                                    </label>
                                    <input
                                        disabled
                                        value={credito.ultima_cuota}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="date" placeholder="" />
                                </div>

                                {/* <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        ganancia de interes
                                    </label>
                                    <input
                                        disabled
                                        value={ganancia}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div> */}
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                    <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                        Informacion de la cuota
                    </div>
                    <div className="p-3">
                        <form className="w-full float-root" onSubmit={updateCuota}>
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Fecha de Pago
                                    </label>
                                    <input
                                        disabled
                                        value={cuota.fecha_couta}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Monto
                                    </label>
                                    <input
                                        disabled
                                        value={cuota.monto}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Interes
                                    </label>
                                    <input
                                        disabled
                                        value={cuota.interes}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Total
                                    </label>
                                    <input
                                        disabled
                                        value={cuota.total}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                            </div>
                            
                            <button className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded float-right mb-5"
                                type="submit">
                                Pagar Cuota
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
     );
}
 
export default ViewCuotaComponent;