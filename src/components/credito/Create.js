import React, { Fragment, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import clienteAxios from '../../config/axios';
import Select from 'react-select'
import MonedaComponent from '../../config/moneda';



const CreateCreditoComponent = () => {

    const navigate = useNavigate()
    const [idCliente, setIdCliente] = useState('')
    const [clientes, setClientes] = useState([])
    const [monto, setMonto] = useState('')
    const [inicial, setInicial] = useState('')
    const [credito, setCredito] = useState('')
    const [interes, setInteres] = useState('')
    const [cuotas, setCuotas] = useState('')
    const [intervalor, setIntervalos] = useState('')
    const [fecha, setFecha] = useState('')
    const [simulado, setSimulado] = useState(false)
    const [detalleCuotas, setDetalleCuotas] = useState('')
    const [ganancia, setGanancia] = useState('')
    const [activar, setActivar] = useState(false)
    const [facturas, setFacturas] = useState([])
    const [idFactura, setFactura] = useState('')

    const getClientes = async () => {
        const res = await clienteAxios.get('/clientes')
        console.log(res.data)
        const options = res.data.map(cliente => {
            return { value: `${cliente.id}`, label: `${cliente.documento} - ${cliente.nombres} ${cliente.apellidos}` };
        })
        setClientes(options)
    }

    const getFacturas = async (value) => {
        setIdCliente(value)
        const res = await clienteAxios.get('/facturas/cliente/' + value)
        console.log(res.data)
        const options = res.data.map(factura => {
            return { value: `${factura.id}`, label: `${factura.numero_documento} - Fecha: ${factura.fecha} - Monto: ${factura.total} - Credito: #${factura.idCredito}` };
        })
        setFacturas(options)
    }

    const logChange = async (val) => {
        setIdCliente(val.value)
        getFacturas(val.value)
    }

    const logChange2 = async (val) => {
        setFactura(val.value)
    }

    const calcularCredito = () => {
        const credito = monto - inicial
        setCredito(credito)
    }

    const simular = async (e) => {
        e.preventDefault()
        const res = await clienteAxios.post('/credito/simular', {
            monto: monto,
            inicial: inicial,
            credito: credito,
            interes: interes,
            cuotas: cuotas,
            intervalos: intervalor,
            fecha: fecha
        })
        console.log(res.data)
        setGanancia(res.data.totalInteres)
        setDetalleCuotas(res.data.coutas)
        setSimulado(true)
        setActivar(true)
    }

    const aprobar = async (e) => {
        e.preventDefault()
        const res = await clienteAxios.post('/credito/generar', {
            monto: monto,
            inicial: inicial,
            credito: credito,
            interes: interes,
            cuotas: cuotas,
            intervalos: intervalor,
            fecha: fecha,
            idCliente: idCliente,
            idFactura: idFactura
        })
        console.log(res.data)
        navigate('/creditos')
    }

    useEffect(() => {
        getClientes()
    }, [])

    return (
        <Fragment>
            <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                    <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                        Crear Credito
                    </div>
                    <div className="p-3">
                        <form className="w-full float-root" >
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-3/3 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Cliente
                                    </label>
                                    <Select
                                        onChange={logChange}
                                        options={clientes}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-3/3 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Factura
                                    </label>
                                    <Select
                                        onChange={logChange2}
                                        options={facturas}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>
                            </div>

                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Monto Total
                                    </label>
                                    <input
                                        onChange={(e) => setMonto(e.target.value)}
                                        value={monto}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Inicial
                                    </label>
                                    <input
                                        onChange={(e) => setInicial(e.target.value)}
                                        value={inicial}
                                        onBlur={calcularCredito}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Monto a acreditar
                                    </label>
                                    <input
                                        disabled
                                        value={credito}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Interes (%)
                                    </label>
                                    <input
                                        onChange={(e) => setInteres(e.target.value)}
                                        value={interes}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                            </div>
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Numero de Cuotas
                                    </label>
                                    <input
                                        onChange={(e) => setCuotas(e.target.value)}
                                        value={cuotas}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Intervalos de Cuota
                                    </label>
                                    <select
                                        onChange={(e) => setIntervalos(e.target.value)}
                                        value={intervalor}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    >
                                        <option value="0">SELECCIONE</option>
                                        <option value="1">DIARIO</option>
                                        <option value="7">SEMANAL</option>
                                        <option value="15">QUINCENAL</option>
                                        <option value="30">MENSUAL</option>
                                    </select>
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        FECHA PRIMERA COUTA
                                    </label>
                                    <input
                                        onChange={(e) => setFecha(e.target.value)}
                                        value={fecha}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="date" placeholder="" />
                                </div>

                                <div className="w-full md:w-1/4 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        ganancia de interes
                                    </label>
                                    <input
                                        disabled
                                        value={ganancia}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                        id="grid-last-name" type="text" placeholder="" />
                                </div>
                            </div>

                            {activar === true && <button onClick={aprobar} className="shadow bg-green-500 hover:bg-green-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded float-left mb-5"
                                type="button">
                                Aprobar Credito
                            </button>}

                            <button onClick={simular} className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded float-right mb-5"
                                type="button">
                                Obtener Cuotas
                            </button>
                        </form>
                    </div>
                </div>
            </div>


            {
                simulado && <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                    <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                        <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                            Cuotas
                        </div>
                        <div className="p-3">
                            <form className="w-full float-root" >
                                <table className="hidden w-full mb-16 text-left table-auto lg:table">
                                    <thead>
                                        <tr>
                                            <th className="py-2 pr-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Fecha</th>
                                            <th className="px-2 py-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Monto</th>
                                            <th className="px-2 py-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Interes</th>
                                            <th className="py-2 pl-2 text-xs font-medium tracking-wider text-right text-gray-500 uppercase leading-4">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {detalleCuotas.map((cuota) => (
                                            <tr key={cuota.id}>
                                                <td className="py-2 pr-2 font-bold whitespace-nowrap"> {cuota.fecha_pago}</td>
                                                <td className="px-2 py-2 whitespace-nowrap"><MonedaComponent /> {cuota.monto}</td>
                                                <td className="px-2 py-2 whitespace-nowrap"><MonedaComponent /> {cuota.interes}</td>
                                                <td className="py-2 pl-2 text-right whitespace-nowrap"><MonedaComponent /> {cuota.total}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            }
        </Fragment>
    );
}

export default CreateCreditoComponent;