import React, { Fragment, useEffect, useState } from 'react'
import clienteAxios from '../../config/axios'
import {Container, Row, Col, Button, Form, Card, Alert} from 'react-bootstrap';
import LoaderComponent from '../adicionales/Loader';
import LoginComponent from '../Login';
import { useNavigate } from 'react-router-dom';

const EditEmpresaComponent = () => {
    const [documento, setDocumento] = useState('')
    const [razon_social, setRazonSocial] = useState('')
    const [telefono, setTelefono] = useState('')
    const [correo, setCorreo] = useState('')
    const [web, setWeb] = useState('')
    const [id, setId] = useState('')
    const [validarDocumento, setValidarDocumento] = useState(false)
    const [alert, setAlert] = useState('')
    const [loaderData, setLoaderData] = useState(true)
    const [dataLoading, setDataLoading] = useState(false)
    const navigate = useNavigate()

    const getEmpresa = async () => {
        clienteAxios.get('/empresa').then((empresa) => {
            setDocumento(empresa.data.documento)
                setRazonSocial(empresa.data.razon_social)
                setTelefono(empresa.data.telefono)
                setCorreo(empresa.data.correo)
                setWeb(empresa.data.web)
                setId(empresa.data.id)
                setDataLoading(true)
                setLoaderData(false)
        }).catch(function (error) {
            console.log(error.response.status) // 401
            navigate('/login')
        }) 
    }

    const update = async (e) => {
        e.preventDefault();
        const empresa = await clienteAxios.post('/empresa', {
            documento: documento,
            razon_social: razon_social,
            telefono: telefono,
            correo: correo,
            web: web,
            id: id
        })

        setAlert(empresa.data.status)

        setTimeout(() => {
            setAlert(false)
        }, 5000)
    }

    useEffect(() => {
        getEmpresa()
    }, [])

    return (
        <Fragment>
            {loaderData && <Container fluid="sm"><LoaderComponent /></Container>}
            {dataLoading &&
                <Container fluid="sm">
                    <Card className="text-left">
                        <Card.Header>Informacion de la Empresa</Card.Header>
                        <form onSubmit={update}>
                            <Card.Body>
                                {alert && <Alert variant="success">
                                    Informacion de la Empresa actualizada con exito!
                                </Alert>}

                                <Row md="2" sm="1" xs="1">
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Documento</Form.Label>
                                            <Form.Control value={documento} isInvalid={validarDocumento} onChange={(e) => setDocumento(e.target.value)} type="text" m />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Razon Social</Form.Label>
                                            <Form.Control value={razon_social} onChange={(e) => setRazonSocial(e.target.value)} type="text" placeholder="Enter email" />
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row md="3" sm="1" xs="1">
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Telefono</Form.Label>
                                            <Form.Control value={telefono} onChange={(e) => setTelefono(e.target.value)} type="text" placeholder="Enter email" />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Correo</Form.Label>
                                            <Form.Control value={correo}
                                                onChange={(e) => setCorreo(e.target.value)} type="email" placeholder="Enter email" />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Pagina WEB</Form.Label>
                                            <Form.Control value={web} onChange={(e) => setWeb(e.target.value)} type="text" placeholder="Enter email" />
                                        </Form.Group>
                                    </Col>
                                </Row>
                            </Card.Body>
                            <Card.Footer className="text-right">
                                <Button variant="success" type={"submit"}> <i className="fa fa-save" /> Actualizar Informacion de la Empresa</Button>
                            </Card.Footer>
                        </form>
                    </Card>
                </Container>}
        </Fragment>
    );
}

export default EditEmpresaComponent;