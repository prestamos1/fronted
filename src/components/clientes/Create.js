import React, { Fragment } from 'react';
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import clienteAxios from "../../config/axios";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import Swal from 'sweetalert2'

const CreateClienteComponent = () => {

    const [tipo_documento, setTipoDocumento] = useState('')
    const [documento, setDocumento] = useState('')
    const [nombres, setNombres] = useState('')
    const [apellidos, setApellidos] = useState('')
    const [direccion, setDireccion] = useState('')
    const [correo, setCorreo] = useState('')
    const [telefono, setTelefono] = useState('')
    const [observaciones, setObservaciones] = useState('')
    const [documento_valido, setDocumentoValido] = useState(false)
    const [isLoading, setLoading] = useState(false)

    const navigate = useNavigate()

    const validarDocumento = async () => {
        const validar = await clienteAxios.get('/cliente/documento/' + documento)

        console.log(validar)
        if (validar.data === 'existe') {
            console.log('existe')
            setDocumentoValido(true)
        }
        if(validar.data === 0) {
            setDocumentoValido(false)
        }
        
    }

    const store = async (e) => {
        setLoading(true)
        e.preventDefault();
        await clienteAxios.post('clientes', {
            tipo_documento: tipo_documento,
            documento: documento,
            nombres: nombres,
            apellidos: apellidos,
            direccion: direccion,
            correo: correo,
            telefono: telefono,
            observaciones: observaciones,
        })

        Swal.fire({
            title: 'Exito!',
            text: 'Cliente registrado con exito!',
            icon: 'success',
            confirmButtonText: 'Cerrar'
        })

        navigate('/clientes')
    }

    return ( 
        <Fragment>
            <Container fluid="sm">
                <Card className="text-left">
                    <Card.Header>Crear Cliente</Card.Header>
                    <form onSubmit={store}>
                        <Card.Body className="float-root">
                            <Row md="2" sm="1" xs="1">
                                <Col>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label>Tipo de Documento</Form.Label>
                                        <Form.Select required value={tipo_documento} onChange={(e) => setTipoDocumento(e.target.value)} aria-label="Default select example">
                                            <option value="2"> CEDULA </option>
                                        </Form.Select>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label>Documento</Form.Label>
                                        <Form.Control required isInvalid={documento_valido} value={documento}
                                            onChange={(e) => setDocumento(e.target.value)}
                                            onBlur={validarDocumento} type="text" />
                                    </Form.Group>
                                </Col>
                            </Row>


                            <Row md="2" sm="1" xs="1">
                                <Col>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label>Nombres</Form.Label>
                                        <Form.Control required value={nombres} onChange={(e) => setNombres(e.target.value)} type="text" />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label>Apellidos</Form.Label>
                                        <Form.Control required value={apellidos} onChange={(e) => setApellidos(e.target.value)}  type="text" />
                                    </Form.Group>
                                </Col>
                            </Row>

                            <Row md="2" sm="1" xs="1">
                                <Col>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label>Correo</Form.Label>
                                        <Form.Control required value={correo} onChange={(e) => setCorreo(e.target.value)} type="email" />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label>Telefono</Form.Label>
                                        <Form.Control required value={telefono} onChange={(e) => setTelefono(e.target.value)}  type="text" />
                                    </Form.Group>
                                </Col>
                            </Row>

                            <Row md="1" sm="1" xs="1">
                                <Col>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label>Direccion</Form.Label>
                                        <Form.Control required value={direccion} onChange={(e) => setDireccion(e.target.value)} type="text" />
                                    </Form.Group>
                                </Col>
                            </Row>

                            <Row md="1" sm="1" xs="1">
                                <Col>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Label>Observaciones</Form.Label>
                                        <Form.Control as="textarea" value={observaciones} onChange={(e) => setObservaciones(e.target.value)} />
                                    </Form.Group>
                                </Col>
                            </Row>

                            
                        </Card.Body>
                        <Card.Footer className="text-right">
                            <Button disabled={isLoading || documento_valido} variant="success" type={"submit"}>
                                <i className='fa fa-save' /> {isLoading ? 'Creando' : 'Crear Usuario'}
                            </Button>
                        </Card.Footer>
                    </form>
                </Card>
            </Container> 
        </Fragment>
     );
}
 
export default CreateClienteComponent;