import React, { Fragment, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import clienteAxios from '../../config/axios';
import { PieChart } from 'react-minimal-pie-chart';
import MonedaComponent from '../../config/moneda';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import LoaderComponent from '../adicionales/Loader';
import Chart from "react-apexcharts";

const ViewClienteComponent = () => {

    const [cliente, setCliente] = useState('')
    const [pagadas, setPagadas] = useState('')
    const [sinpagar, setSinPagar] = useState('')
    const [mora, setMora] = useState('')
    const [totalCuotas, setTotalCuotas] = useState('')
    const [cuotas, setCuotas] = useState([])
    const [cuotas_mora, setCuotasMora] = useState([])
    const [creditos, setCreditos] = useState([])
    const [historial, setHistorial] = useState([])
    const [dataLoading, setDataLoading] = useState(false)
    const { id } = useParams()

    const optionsData = {
        options: {
            labels: ['Sin Pagar', 'Pagadas', 'En Mora'],
            legend: {
                show: true,
                position: 'top',
            },
        },
        series: [sinpagar, pagadas, mora],
    }


    const getCliente = async () => {
        const res = await clienteAxios.get('/cliente/detalle/' + id)
        setCliente(res.data.cliente)
        setPagadas(res.data.pagadas)
        setSinPagar(res.data.sinpagar)
        setTotalCuotas(res.data.totalCuotas)
        setCuotas(res.data.cuotas)
        setCuotasMora(res.data.coutas_mora)
        setCreditos(res.data.creditos)
        setHistorial(res.data.historia_pagos)
        setMora(res.data.mora)
        setDataLoading(true)
    }

    const pagar = (valor) => {
        console.log(valor)
    }

    useEffect(() => {
        getCliente()
    }, [])


    return (
        <Fragment>
            {dataLoading === false && <Container fluid="sm"><LoaderComponent /></Container>}
            {dataLoading &&
                <Fragment>
                    <Container fluid="sm" className="mb-3">
                        <Card className="text-left">
                            <Card.Header>Informacion del Cliente</Card.Header>
                            <form >
                                <Card.Body className="float-root">
                                    <Row md="4" sm="1" xs="1">
                                        <Col>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label>Tipo de Documento</Form.Label>
                                                <Form.Select disabled value={cliente.tipo_documento} aria-label="Default select example">
                                                    <option value="">Seleccione</option>
                                                    <option value="1"> DNI </option>
                                                    <option value="2"> CE </option>
                                                    <option value="3"> PASAPORTE </option>
                                                    <option value="4"> RUC </option>
                                                </Form.Select>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label>Documento</Form.Label>
                                                <Form.Control disabled value={cliente.documento}
                                                    type="text" />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label>Nombres</Form.Label>
                                                <Form.Control disabled value={cliente.nombres} type="text" />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label>Apellidos</Form.Label>
                                                <Form.Control disabled value={cliente.apellidos} type="text" />
                                            </Form.Group>
                                        </Col>
                                    </Row>


                                    <Row md="3" sm="1" xs="1">
                                        <Col>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label>Correo</Form.Label>
                                                <Form.Control disabled value={cliente.correo} type="email" />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label>Telefono</Form.Label>
                                                <Form.Control disabled value={cliente.telefono} type="text" />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label>Direccion</Form.Label>
                                                <Form.Control disabled value={cliente.direccion} type="text" />
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </form>
                        </Card>
                        {
                            mora === 0 &&
                            <Card bg="success text-white mt-3" >
                                <Card.Header className='font-bold'>Al dia</Card.Header>
                                <Card.Body className="bg-white" border="success">
                                    <Card.Text className="text-dark">
                                        Este cliente no cuenta con cuotas en Mora
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                            ||
                            <Card bg="danger text-white mt-3">
                                <Card.Header>Con Mora</Card.Header>
                                <Card.Body className="bg-white" border="danger">
                                    <Card.Text className="text-dark">
                                        Este cliente tiene {mora} cuotas sin pagar
                                    </Card.Text>
                                    <Table responsive>
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>C</th>
                                                <th>Fecha</th>
                                                <th>Monto</th>
                                                <th>Interes</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {cuotas_mora.map((cuota) => (
                                                <tr key={cuota.id}>
                                                    <td className="py-2 pr-2 whitespace-nowrap pb-6">
                                                        {(() => {
                                                            if (cuota.estadoCuota === 1) {
                                                                return <Button as={Link} to={`/cuota/pagar/${cuota.id}`} variant="warning">
                                                                    <i className='fa fa-save' /> Pagar
                                                                </Button>
                                                            }

                                                            else if (cuota.estadoCuota === 2) {
                                                                return <span class="bg-green-500 text-white text-xs font-semibold mr-2 px-4 pt-2 pb-2 py-0.5 rounded dark:bg-green-200 dark:green-red-900">Pagado</span>
                                                            }

                                                            else if (cuota.estadoCuota === 3) {
                                                                return <Fragment>
                                                                    <Button as={Link} to={`/cuota/pagar/${cuota.id}`} variant="danger">
                                                                        <i class="fas fa-receipt text-white ml-2 mr-2"></i> Pagar
                                                                    </Button>
                                                                    
                                                                    <i className="fas fa-exclamation-circle text-danger ml-2"></i>
                                                                </Fragment>
                                                            }
                                                        })()}

                                                    </td>
                                                    <td className="className='underline text-blue-500 text-center'"> <Link to={`/credito/${cuota.credito.id}`}><i className='fa fa-info-circle'></i></Link></td>
                                                    <td className="py-2 pr-2 font-bold whitespace-nowrap"> {cuota.fecha_couta}</td>
                                                    <td className="px-2 py-2 whitespace-nowrap"><MonedaComponent /> {cuota.monto}</td>
                                                    <td className="px-2 py-2 whitespace-nowrap"><MonedaComponent /> {cuota.interes}</td>
                                                    <td className="py-2 pl-2 text-right whitespace-nowrap"><MonedaComponent /> {cuota.total}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </Table>
                                </Card.Body>
                            </Card>
                        }

                        <Row md="2" sm="1" xs="1" className="mt-3">
                            <Col md="3">
                                <Card>
                                    <Card.Header>Porcentajes</Card.Header>
                                    <Card.Body>
                                        <Chart options={optionsData.options} series={optionsData.series} type="donut" width={"100%"} />
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col md="9">
                                <Card>
                                    <Card.Header>Proximas 7 Cuotas</Card.Header>
                                    <Card.Body>
                                        <Table responsive>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>C</th>
                                                    <th>Fecha</th>
                                                    <th>Monto</th>
                                                    <th>Interes</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {cuotas.map((cuota) => (
                                                    <tr key={cuota.id}>
                                                        <td className="py-2 pr-2 whitespace-nowrap pb-6">
                                                            {(() => {
                                                                if (cuota.estadoCuota === 1) {
                                                                    return <Button as={Link} to={`/cuota/pagar/${cuota.id}`} variant="warning">
                                                                        <i class="fas fa-receipt text-white ml-2 mr-2"></i> Pagar
                                                                    </Button>
                                                                }

                                                                else if (cuota.estadoCuota === 2) {
                                                                    return <span class="bg-green-500 text-white text-xs font-semibold mr-2 px-4 pt-2 pb-2 py-0.5 rounded dark:bg-green-200 dark:green-red-900">Pagado</span>
                                                                }

                                                                else if (cuota.estadoCuota === 3) {
                                                                    return <Fragment>
                                                                        <Button as={Link} to={`/cuota/pagar/${cuota.id}`} variant="danger">
                                                                            <i class="fas fa-receipt text-white ml-2 mr-2"></i> Pagar
                                                                        </Button>

                                                                        <i className="fas fa-exclamation-circle text-danger ml-2"></i>
                                                                    </Fragment>
                                                                }
                                                            })()}

                                                        </td>
                                                        <td className="className='underline text-blue-500 text-center'"> <Link to={`/credito/${cuota.credito.id}`}><i className='fa fa-info-circle'></i></Link></td>
                                                        <td className="py-2 pr-2 font-bold whitespace-nowrap"> {cuota.fecha_couta}</td>
                                                        <td className="px-2 py-2 whitespace-nowrap"><MonedaComponent /> {cuota.monto}</td>
                                                        <td className="px-2 py-2 whitespace-nowrap"><MonedaComponent /> {cuota.interes}</td>
                                                        <td className="py-2 pl-2 text-right whitespace-nowrap"><MonedaComponent /> {cuota.total}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </Table>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>

                        <Row md="2" sm="1" xs="1" className="mt-3">
                            <Col md="6">
                                <Card >
                                    <Card.Header>Historial de Creditos</Card.Header>
                                    <Card.Body>
                                        <Table responsive>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Monto</th>
                                                    <th>Interes</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    creditos.map((credito) => (
                                                        <tr key={credito.id}>
                                                            <td className="className='underline text-blue-500 text-center'"> <Link to={`/credito/${credito.id}`}><i className='fa fa-info-circle'></i></Link></td>
                                                            <td><MonedaComponent /> {credito.credito}</td>
                                                            <td>{credito.interes} %</td>
                                                            <td><MonedaComponent /> {credito.credito / 100 * 18 + credito.credito}</td>
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                        </Table>
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col md="6">
                                <Card >
                                    <Card.Header>Historial de Creditos</Card.Header>
                                    <Card.Body>
                                        <Card.Text >
                                            <Table responsive>
                                                <thead>
                                                    <tr>
                                                        <th>Monto Cuota</th>
                                                        <th>Fecha Cuota</th>
                                                        <th>Fecha Pago</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        historial.map((hito) => (
                                                            <tr key={hito.id}>
                                                                <td><MonedaComponent /> {hito.monto}</td>
                                                                <td>{hito.fecha_couta}</td>
                                                                <td>{hito.fecha_pago}</td>
                                                            </tr>
                                                        ))
                                                    }
                                                </tbody>
                                            </Table>
                                            
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </Fragment>
            }
        </Fragment>
    );
}

export default ViewClienteComponent;