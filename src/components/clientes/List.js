import React, { useEffect, useState, Fragment } from 'react'
import { Link } from 'react-router-dom'
import clienteAxios from '../../config/axios'
import {Container, InputGroup, Form} from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import DataTable from 'react-data-table-component'
import LoaderComponent from '../adicionales/Loader'
import Badge from 'react-bootstrap/Badge'

const ListClientesComponent = () => {
  const [clientes, setCliente] = useState([])
  const [pending, setPending] = React.useState(true)

  const columns = [
    {
      name: '#',
      selector: (row) => row.id,
      width: '40px',
    },
    {
      name: 'Documento',
      selector: (row) => row.documento,
    },
    {
      name: 'Nombre',
      selector: (row) => row.nombres + ' ' + row.apellidos,
    },
    {
      name: 'Estado',
      button: true,
      cell: (row) =>
        (row.estadoCliente === 1 && <Badge bg="success">Activo</Badge>) || (
          <Badge bg="danger">Inactivo</Badge>
        ),
    },
    {
      name: '',
      button: true,
      cell: (row) => (
        <Fragment>
          <Button
            as={Link}
            to={`/cliente/${row.id}`}
            variant="info"
            className="mr-2 text-white"
          >
            <i className="fas fa-eye"></i>
          </Button>
          <Button
            as={Link}
            to={`/cliente/edit/${row.id}`}
            variant="success"
            className="mr-2"
          >
            <i className="fas fa-edit"></i>
          </Button>
        </Fragment>
      ),
    },
  ]

  const paginationComponentOptions = {
    rowsPerPageText: 'Filas por página',
    rangeSeparatorText: 'de',
    selectAllRowsItem: true,
    selectAllRowsItemText: 'Todos',
  }

  const getClientes = async () => {
    const res = await clienteAxios.get('/clientes')
    setCliente(res.data)
    setPending(false)
  }

  const buscar = async (e) => {
    setPending(true)
    const res = await clienteAxios.get('/clientes/buscar/'+e)
    setCliente(res.data.data)
    setPending(false)
  }

  useEffect(() => {
    getClientes()
  }, [])

  return (
    <Container fluid="sm">
      <Card className="text-left">
        <Card.Header>Clientes</Card.Header>
        <Card.Body className="float-root">
          <Button
            as={Link}
            to={'/cliente/create'}
            variant="success"
            className="float-right mb-2"
          >
            <i className="fa fa-plus-circle" /> Agregar Cliente
          </Button>
          <InputGroup className="mb-3">
            <InputGroup.Text id="basic-addon1">
              <i className="fas fa-search"></i>
            </InputGroup.Text>
            <Form.Control
              placeholder="Buscador (Puede buscar NOMBRE, APELLIDOS O DOCUMENTO!)"
              aria-label="Buscador (Puede buscar NOMBRE, APELLIDOS O DOCUMENTO!)"
              aria-describedby="basic-addon1"
              onKeyPress={() => buscar()}
              onChange={(e) => buscar(e.target.value)}
            />
          </InputGroup>
          <DataTable
            columns={columns}
            data={clientes}
            responsive="true"
            progressPending={pending}
            progressComponent={<LoaderComponent />}
            pagination
            paginationComponentOptions={paginationComponentOptions}
          />
        </Card.Body>
      </Card>
    </Container>
  )
}

export default ListClientesComponent
