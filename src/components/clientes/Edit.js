import React, { Fragment } from 'react';
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import clienteAxios from "../../config/axios";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import LoaderComponent from '../adicionales/Loader';

const EditCLienteComponent = () => {

    const [tipo_documento, setTipoDocumento] = useState('')
    const [documento, setDocumento] = useState('')
    const [nombres, setNombres] = useState('')
    const [apellidos, setApellidos] = useState('')
    const [direccion, setDireccion] = useState('')
    const [correo, setCorreo] = useState('')
    const [telefono, setTelefono] = useState('')
    const [observaciones, setObservaciones] = useState('')
    const [estadoCliente, setEstadoCliente] = useState('')
    const [isLoading, setLoading] = useState(false)
    const [dataLoading, setDataLoading] = useState(false)
    const [documento_valido, setDocumentoValido] = useState(false)
    const { id } = useParams()

    const navigate = useNavigate()

    const validarDocumento = async () => {
        const validar = await clienteAxios.get('/cliente/documento/' + documento)

        console.log(validar)
        if (validar.data === 'existe') {
            console.log('existe')
            setDocumentoValido(true)
        }
        if (validar.data === 0) {
            setDocumentoValido(false)
        }

    }

    const getCliente = async () => {
        const res = await clienteAxios.get('/cliente/' + id)
        setTipoDocumento(res.data.tipo_documento)
        setDocumento(res.data.documento)
        setNombres(res.data.nombres)
        setApellidos(res.data.apellidos)
        setDireccion(res.data.direccion)
        setCorreo(res.data.correo)
        setTelefono(res.data.telefono)
        setObservaciones(res.data.observaciones)
        setEstadoCliente(res.data.estadoCliente)
        setDataLoading(true)
    }

    useEffect(() => {
        getCliente()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    

    const update = async (e) => {
        setLoading(true)
        e.preventDefault();
        await clienteAxios.post('cliente', {
            tipo_documento: tipo_documento,
            documento: documento,
            nombres: nombres,
            apellidos: apellidos,
            direccion: direccion,
            correo: correo,
            telefono: telefono,
            observaciones: observaciones,
            estadoCliente: estadoCliente,
            id: id
        })

        navigate('/clientes')
    }

    return (
        <Fragment>
            {dataLoading === false && <Container fluid="sm"><LoaderComponent /></Container>}
            {dataLoading &&
                <Container fluid="sm">
                    <Card className="text-left">
                        <Card.Header>Actualizar Cliente</Card.Header>
                        <form onSubmit={update}>
                            <Card.Body className="float-root">
                                <Row md="2" sm="1" xs="1">
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Tipo de Documento</Form.Label>
                                            <Form.Select required value={tipo_documento} onChange={(e) => setTipoDocumento(e.target.value)} aria-label="Default select example">
                                            <option value="2"> CEDULA </option>
                                            </Form.Select>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Documento</Form.Label>
                                            <Form.Control required isInvalid={documento_valido} value={documento}
                                                onChange={(e) => setDocumento(e.target.value)}
                                                onBlur={validarDocumento} type="text" />
                                        </Form.Group>
                                    </Col>
                                </Row>


                                <Row md="2" sm="1" xs="1">
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Nombres</Form.Label>
                                            <Form.Control required value={nombres} onChange={(e) => setNombres(e.target.value)} type="text" />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Apellidos</Form.Label>
                                            <Form.Control required value={apellidos} onChange={(e) => setApellidos(e.target.value)} type="text" />
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row md="2" sm="1" xs="1">
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Correo</Form.Label>
                                            <Form.Control required value={correo} onChange={(e) => setCorreo(e.target.value)} type="email" />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Telefono</Form.Label>
                                            <Form.Control required value={telefono} onChange={(e) => setTelefono(e.target.value)} type="text" />
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row md="1" sm="1" xs="1">
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Direccion</Form.Label>
                                            <Form.Control required value={direccion} onChange={(e) => setDireccion(e.target.value)} type="text" />
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row md="1" sm="1" xs="1">
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Observaciones</Form.Label>
                                            <Form.Control as="textarea" value={observaciones} onChange={(e) => setObservaciones(e.target.value)} />
                                        </Form.Group>
                                    </Col>
                                </Row>


                            </Card.Body>
                            <Card.Footer className="text-right">
                                <Button disabled={isLoading || documento_valido} variant="success" type={"submit"}>
                                    <i className='fa fa-save' /> {isLoading ? 'Actualizando' : 'Actualizar Usuario'}
                                </Button>
                            </Card.Footer>
                        </form>
                    </Card>
                </Container>
            }
        </Fragment>
    );
}

export default EditCLienteComponent;