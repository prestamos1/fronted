import React, { Fragment, useEffect, useState } from 'react'
import clienteAxios from '../../config/axios'
import sha1 from 'sha1'
import { useNavigate, useParams } from "react-router-dom";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import LoaderComponent from '../adicionales/Loader';

const EditUserComponent = () => {

    const [name, setName] = useState('')
    const [user, setUser] = useState('')
    const [isUser, setIdUser] = useState('')
    const [type_user, setTypeUser] = useState('')
    const [password, setPassword] = useState('')
    const [isLoading, setLoading] = useState(false)
    const [isLoading2, setLoading2] = useState(false)
    const [dataLoading, setDataLoading] = useState(false)
    const navigate = useNavigate()
    const { id } = useParams()

    const update = async (e) => {
        setLoading(true)
        e.preventDefault();
        const crear = await clienteAxios.post('/usuario', {
            name: name,
            user: user,
            type_user: type_user,
            id: isUser
        })

        if (crear.data.status === true) {
            navigate('/usuarios')
        }

    }

    const updatePassword = async (e) => {
        setLoading2(true)
        e.preventDefault()

        const update = await clienteAxios.post('/usuario/clave', {
            id: isUser,
            password: password,
        })

        if (update.data.status === true) {
            navigate('/usuarios')
        }
    }

    const getUsuario = async () => {
        const data = await clienteAxios.get('/usuario/' + id)
        setName(data.data.name)
        setUser(data.data.email)
        setTypeUser(data.data.typeUser)
        setIdUser(data.data.id)
        setDataLoading(true)
    }

    useEffect(() => {
        getUsuario()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <Fragment>
            {dataLoading === false && <Container fluid="sm"><LoaderComponent /></Container>}
            {dataLoading &&
                <Container fluid="sm">
                    <Card className="text-left mb-10">
                        <Card.Header>Editar Usuario</Card.Header>
                        <form onSubmit={update}>
                            <Card.Body className="float-root">
                                <Row md="2" sm="1" xs="1">
                                    <Col md="9">
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Usuario</Form.Label>
                                            <Form.Control required value={user} onChange={(e) => setUser(e.target.value)} type="text" m />
                                        </Form.Group>
                                    </Col>
                                    <Col md="3">
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Clave</Form.Label>
                                            <Form.Select required value={type_user} onChange={(e) => setTypeUser(e.target.value)} aria-label="Default select example">
                                                <option>Seleccione</option>
                                                <option value="1"> Administrador</option>
                                                <option value="2"> Gestor </option>
                                            </Form.Select>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row md="1" sm="1" xs="1">
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Nombre</Form.Label>
                                            <Form.Control required value={name} onChange={(e) => setName(e.target.value)} type="text" m />
                                        </Form.Group>
                                    </Col>
                                </Row>
                            </Card.Body>
                            <Card.Footer className="text-right">
                                <Button disabled={isLoading} variant="success" type={"submit"}>
                                    <i className='fa fa-save' /> {isLoading ? 'Actualizando' : 'Actualizar Usuario'}
                                </Button>
                            </Card.Footer>
                        </form>
                    </Card>
                    <Card className="text-left">
                        <Card.Header>Nueva Clave</Card.Header>
                        <form onSubmit={updatePassword}>
                            <Card.Body className="float-root">
                                <Row md="" sm="1" xs="1">
                                    <Col>
                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Nueva Clave</Form.Label>
                                            <Form.Control required value={password} onChange={(e) => setPassword(e.target.value)} type="text" m />
                                        </Form.Group>
                                    </Col>
                                </Row>
                            </Card.Body>
                            <Card.Footer className="text-right">
                                <Button disabled={isLoading2} variant="success" type={"submit"}>
                                    <i className='fa fa-save' /> {isLoading2 ? 'Asignando Clave' : 'Asignar Clave'}
                                </Button>
                            </Card.Footer>
                        </form>
                    </Card>
                </Container>}
        </Fragment>
    );
}

export default EditUserComponent;