import React, { useState, Fragment, useEffect } from 'react'
import clienteAxios from '../../config/axios'
import { useNavigate } from 'react-router-dom'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';

const CreateUsuarioComponent = () => {

    const [name, setName] = useState('')
    const [user, setUser] = useState('')
    const [password, setPassword] = useState('')
    const [type_user, setTypeUser] = useState('')
    const [isLoading, setLoading] = useState(false)
    const navigate = useNavigate() 


    const store = async (e) => {
        setLoading(true)
        e.preventDefault();
        await clienteAxios.post('/usuarios', {
            name: name,
            user: user,
            password: password,
            type_user: type_user,
        })
        navigate('/usuarios')
    }


    return ( 
        <Fragment>
            <Container fluid="sm">
                <Card className="text-left">
                    <Card.Header>Crear Usuario</Card.Header>
                    <form onSubmit={store}>
                    <Card.Body className="float-root">
                        <Row md="2" sm="1" xs="1">
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Usuario</Form.Label>
                                    <Form.Control required value={user} onChange={(e) => setUser(e.target.value)} type="text" m />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Clave</Form.Label>
                                        <Form.Control required value={password} onChange={(e) => setPassword(e.target.value)} type="text" />
                                </Form.Group>
                            </Col>
                        </Row>

                        <Row md="3" sm="1" xs="1">
                            <Col md="9">
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Nombre</Form.Label>
                                        <Form.Control required value={name} onChange={(e) => setName(e.target.value)} type="text" m />
                                </Form.Group>
                            </Col>
                            <Col md="3">
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Clave</Form.Label>
                                        <Form.Select required value={type_user} onChange={(e) => setTypeUser(e.target.value)} aria-label="Default select example">
                                        <option>Seleccione</option>
                                        <option value="1"> Administrador</option>
                                        <option value="2"> Gestor </option>
                                    </Form.Select>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer className="text-right">
                            <Button disabled={isLoading} variant="success" type={"submit"}>
                                <i className='fa fa-save' /> {isLoading ? 'Creando' : 'Crear Usuario'}
                            </Button>
                    </Card.Footer>
                    </form>
                </Card>
            </Container>
        </Fragment>
     );
}
 
export default CreateUsuarioComponent;