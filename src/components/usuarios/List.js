import React, { Fragment, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import clienteAxios from '../../config/axios'
import { Container, Button, Card, Alert, Badge} from 'react-bootstrap';
import DataTable from 'react-data-table-component';
import LoaderComponent from '../adicionales/Loader';


const ListUsuariosComponent = () => {

    const [usuarios, setUsuario] = useState([])
    const [eliminar, setEliminar] = useState('')
    const [pending, setPending] = React.useState(true);
    


    const getUsuarios = async () => {
        const res = await clienteAxios.get('/usuarios')
        setUsuario(res.data)
        setPending(false);
        console.log(res.data)
    }

    const deleteUser = async (id) => {
        const res = await clienteAxios.get(`/usuariod/${id}`)
        if (res.data.status === true) {
            getUsuarios()
            setEliminar(true)

            setTimeout(() => {
                setEliminar(false)
            }, 5000)
        }
    }


    const columns = [
        {
            name: '#',
            selector: row => row.id,
            width: '40px'
        },
        {
            name: 'Nombre',
            selector: row => row.name,
        },
        {
            name: 'Usuario',
            selector: row => row.email,
        },
        {
            name: 'Estado',
            button: true,
            cell: row => (
                row.statusUser === 1
                &&
                (<Badge bg="success">Activo</Badge> || <Badge bg="danger">Inactivo</Badge>)
            ),
        },
        {
            name: 'Tipo de Usuario',
            button: true,
            cell: row => (
                row.typeUser === 1
                &&
                <Badge bg="success">Administrador</Badge> || <Badge bg="primary">Gestor</Badge>
            ),
        },
        {
            name: '',
            button: true,
            cell: row => (
                <Fragment>
                    <Button as={Link} to={`/usuario/edit/${row.id}`} variant="success" className="mr-2"><i className="fas fa-edit"></i></Button>
                    <Button onClick={() => deleteUser(row.id)} variant="danger"><i className="fas fa-trash"></i></Button>
                </Fragment>
            ),
        },
    ];

    const paginationComponentOptions = {
        rowsPerPageText: 'Filas por página',
        rangeSeparatorText: 'de',
        selectAllRowsItem: true,
        selectAllRowsItemText: 'Todos',
    };

    const myNewTheme = {
        rows: {
            fontSize: '25px'
        }
    };

    useEffect(() => {
        getUsuarios()
    }, [])

    return (
        <Fragment>
            <Container fluid="sm">
                <Card className="text-left">
                    <Card.Header>Usuarios</Card.Header>
                    <Card.Body className="float-root">
                        <Button as={Link} to={"/usuario/create"} variant="success" className="float-right"><i className='fa fa-plus-circle' /> Agregar Usuario</Button>
                        {eliminar &&
                            <Alert variant={"success"}>
                                Usuario Eliminado con exito!
                            </Alert>
                        }
                        <DataTable
                            columns={columns}
                            data={usuarios}
                            responsive="true"
                            progressPending={pending}
                            progressComponent={<LoaderComponent />}
                            pagination
                            paginationComponentOptions={paginationComponentOptions}
                            customTheme={myNewTheme}
                        />

                    </Card.Body>
                </Card>
            </Container>
        </Fragment>
    );
}

export default ListUsuariosComponent;