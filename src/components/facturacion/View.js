import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import clienteAxios from '../../config/axios';
import MonedaComponent from '../../config/moneda';
import { Container, Button, Card, Alert, InputGroup, Form, Badge } from 'react-bootstrap'

const ViewFacturaComponent = () => {

    const [factura, setFactura] = useState([])
    const [cliente, setCliente] = useState([])
    const [relaciones, setRelaciones] = useState([])
    const { id } = useParams()

    const getFactura = async () => {
        const res = await clienteAxios.get('/factura/detalle/' + id)
        setFactura(res.data.factura)
        setCliente(res.data.cliente)
        setRelaciones(res.data.relacion)
        console.log(res.data)
    }

    useEffect(() => {
      getFactura()
    }, [])
    
    return (
        <Container>
            <Card>
                <Card.Header>Datos de la Factura</Card.Header>
                <Card.Body>
                <div className="w-full p-4 mb-4 rounded-lg bg-white border border-gray-100 dark:bg-gray-900 dark:border-gray-800">
                        <div className="p-4">
                            <div className="flex flex-row items-center justify-between mb-16">
                                <div className="flex flex-col">
                                    <span className="text-4xl font-bold text-blue-500 uppercase">Factura</span>
                                    <span className="text-gray-500"># { factura.numero_documento }</span>
                                </div>
                                {/* <div className="flex flex-row items-center justify-start text-base font-bold tracking-wider uppercase whitespace-nowrap">
                                    <div className="flex flex-col items-center justify-start text-blue-500 lg:flex-row space-y-1 lg:space-y-0 space-x-2">
                                        <svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" className="text-blue-500 stroke-current" height="28" width="28" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                            <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                            <line x1="12" y1="22.08" x2="12" y2="12"></line>
                                        </svg>
                                        <span>Logo</span>
                                    </div>
                                </div> */}
                            </div>
                            <div className="flex flex-row items-center justify-between mb-16">
                                <div className="flex flex-col">
                                    <span className="font-bold">Factura a:</span>
                                    <span className="text-gray-500">Razon Social: {cliente.nombres} {cliente.apellidos}</span>
                                    <span className="text-gray-500">Documento: {cliente.documento}</span>
                                    <span className="text-gray-500">Telefono: {cliente.telefono} </span>
                                </div>
                                <div className="flex flex-col">
                                    <span className="font-bold">Fecha:</span>
                                    <span className="text-gray-500">{factura.fecha}</span>
                                </div>
                            </div>
                            <table className="hidden w-full mb-16 text-left table-auto lg:table">
                                <thead>
                                    <tr>
                                        <th className="py-2 pr-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Producto</th>
                                        <th className="px-2 py-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Cantidad</th>
                                        <th className="px-2 py-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Precio</th>
                                        <th className="py-2 pl-2 text-xs font-medium tracking-wider text-right text-gray-500 uppercase leading-4">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {relaciones.map((relacion) => (
                                        <tr key={relacion.id}>
                                            <td className="py-2 pr-2 font-bold whitespace-nowrap">
                                            {relacion.producto.marca} - {relacion.producto.modelo} - {relacion.producto.color} - {relacion.producto.chasis}
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap">{relacion.cantidad}</td>
                                            <td className="px-2 py-2 whitespace-nowrap">  <MonedaComponent /> {relacion.precio}</td>
                                            <td className="py-2 pl-2 text-right whitespace-nowrap">  <MonedaComponent /> {relacion.precio * relacion.cantidad}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                            
                            <div className="grid grid-cols-1 gap-y-6 gap-x-6 sm:grid-cols-12">
                                {/* <div className="sm:col-span-6 space-y-2"><div className="font-bold">Detalle del Pago</div>
                                    <table className="w-full text-left table-auto">
                                        <tbody>
                                            <tr>
                                                <td className="py-2 pr-3 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Tipo de Pago</td>
                                                <td className="py-2 pl-3 whitespace-nowrap">{
                                                    (() => {
                                                        if (factura.estadoFactura === 1) {
                                                            return <span>Visa / Mastercard (Tarjeta de Debido o Credito)</span>
                                                        }
                                                        else if (factura.estadoFactura === 2) {
                                                            return <span>Efectivo</span>
                                                        }
                                                        else {
                                                            return <span>Transferencia</span>
                                                        }
                                                    })
                                                }</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div> */}
                                <div className="sm:col-span-6">
                                    <div className="flex flex-col w-full text-left md:text-right space-y-2">
                                        <span className="text-gray-500">Sub Total:  <MonedaComponent /> {factura.subtotal}</span>
                                        <span className="text-gray-500">IGV:  <MonedaComponent /> {factura.igv}</span>
                                        <span className="font-bold">Total</span>
                                        <span className="text-4xl font-bold text-blue-500"> <MonedaComponent /> {factura.total}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Card.Body>
            </Card>
        </Container>
    );
}

export default ViewFacturaComponent;