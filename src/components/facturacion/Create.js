import React, { Fragment } from 'react';
import { useEffect, useState } from "react";
import Select from 'react-select'
import Modal from 'react-modal';
import clienteAxios from "../../config/axios";
import { useNavigate } from 'react-router-dom';
import MonedaComponent from '../../config/moneda';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

const CreateFacturaComponent = () => {

    const [idCliente, setIdCliente] = useState('')
    const [idFactura, setIdFactura] = useState('')
    const [clientes, setCliente] = useState([])
    const [numeroDocumento, setFactura] = useState('')
    const [client, setClient] = useState('')
    const [buscar_cliente, setBuscar] = useState(true)
    const [data_factura, setDataFactura] = useState(false)
    const [fecha_factura, setFechaFactura] = useState('')
    const [sub_total, setSubTotal] = useState('')
    const [igv, setIgv] = useState('')
    const [total, setTotal] = useState('')
    const [productos, setProductos] = useState('')
    const [idProducto, setIdProducto] = useState('')
    const [precio, setPrecio] = useState('')
    const [cantidad, setCantidad] = useState('')
    const [relaciones, setRelacion] = useState([])
    const [metodo_pago, setMetodoPago] = useState('')
    const [observaciones, setObservaciones] = useState('')

    const navigate = useNavigate()

    const getClientes = async () => {
        const res = await clienteAxios.get('/clientes')
        console.log(res.data)
        const options = res.data.map(cliente => {
            return { value: `${cliente.id}`, label: `${cliente.documento} - ${cliente.nombres} ${cliente.apellidos}` };
        })
        setCliente(options)
    }

    const getProductos = async () => {
        const res = await clienteAxios.get('/productos')

        const options = res.data.map(producto => {
            return { key: `${producto.id}`, label: `${producto.producto} - ${producto.marca} - ${producto.modelo} - ${producto.ano} - ${producto.color} - ${producto.chasis} - ${producto.motor} - ${producto.cilindraje} - ${producto.ramv}` };
        })

        setProductos(options)
    }

    const store = async (e) => {
        e.preventDefault()
    }

    const cerrarFactura = async (e) => {
        e.preventDefault()

        const res = await clienteAxios.post('/factura/final', {
            idFactura: idFactura,
            observaciones: observaciones,
            tipoPago: metodo_pago
        });

        if (res.data.status === true) {
            navigate('/facturas')
        }
    }

    const storeRelacion = async (e) => {
        e.preventDefault()
        const res = await clienteAxios.post('relacion/store', {
            idProducto: idProducto.key,
            idFactura: idFactura,
            cantidad: cantidad,
            precio: precio
        })

        console.log(res.data)

        setRelacion(res.data.data)
        setTotal(res.data.total.toFixed(2))
        setIgv(res.data.igv.toFixed(2))
        setSubTotal(res.data.subTotal.toFixed(2))

        setCantidad('')
        setPrecio('')
    }


    const logChange = async (val) => {
        setIdCliente(val.value)
        console.log(numeroDocumento)
        const facturas = await clienteAxios.post('/facturas', {
            idCliente: val.value
        })
        setFactura(facturas.data.numero_documento)
        setClient(facturas.data.cliente.documento + ' - ' + facturas.data.cliente.nombres + ' ' + facturas.data.cliente.apellidos)
        setFechaFactura(facturas.data.fecha)
        setTotal(facturas.data.total)
        setIgv(facturas.data.igv)
        setSubTotal(facturas.data.subtotal)
        setIdFactura(facturas.data.id)
        setBuscar(false)
        setDataFactura(true)
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: '40%',
        },
    };


    const [modalIsOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    Modal.setAppElement('#root');

    useEffect(() => {
        getClientes()
        getProductos()
    }, [])


    return (
        <Fragment>
            <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                    <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                        Crear Factura
                    </div>
                    {buscar_cliente && <div className="p-3">
                        <form className="w-full float-root" onSubmit={store}>
                            <div className="flex flex-wrap -mx-3 mb-6">

                                <div className="w-full md:w-3/3 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Cliente
                                    </label>
                                    <Select
                                        name="idCliente"
                                        value={idCliente}
                                        onChange={logChange}
                                        options={clientes}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>
                            </div>
                        </form>
                    </div>}

                    {data_factura && <div className="p-3">
                        <form className="w-full float-root" onSubmit={storeRelacion}>
                            <div className="flex flex-wrap -mx-3 mb-6">

                                <div className="w-full md:w-2/3 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Cliente
                                    </label>
                                    <Form.Control
                                        disabled
                                        value={client}
                                        type="text"  />
                                </div>

                                <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Numero de Documento
                                    </label>
                                    <input
                                        disabled
                                        value={numeroDocumento}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 mb-6">

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Fecha Hora
                                    </label>
                                    <input
                                        disabled
                                        value={fecha_factura}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Sub Total
                                    </label>
                                    <input
                                        disabled
                                        value={sub_total}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Total Igv (18%)
                                    </label>
                                    <input
                                        disabled
                                        value={igv}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >
                                        Total Factura
                                    </label>
                                    <input
                                        disabled
                                        value={total}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>
                            </div>

                            <div className="flex flex-wrap -mx-3 mt-10">

                                <div className="w-full md:w-2/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Productos
                                    </label>
                                    <Select
                                        onChange={value => setIdProducto(value)}
                                        options={productos}
                                    />
                                </div>

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Precio
                                    </label>
                                    <input
                                        value={precio}
                                        onChange={(e) => setPrecio(e.target.value)}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>

                                <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                    >Cantidad
                                    </label>
                                    <input
                                        onChange={(e) => setCantidad(e.target.value)}
                                        value={cantidad}
                                        className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                    />
                                </div>
                            </div>
                            <Button variant="primary" type="submit" className="float-right mb-5 mt-5">Agregar Producto</Button>


                            <button
                                onClick={openModal}
                                className="shadow bg-red-500 hover:bg-red-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded float-left mb-5 mt-5"
                                type="button">
                                Cerrar Factura
                            </button>
                        </form>
                    </div>}
                </div>
            </div>
            {data_factura && <div className="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                <div className="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                    <div className="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                        Produtos en la Factura
                    </div>
                    <div className="w-full float-root">
                        <div className="flex flex-wrap -mx-3 pl-5 pr-5">
                            <table className="hidden w-full mb-16 text-left table-auto lg:table">
                                <thead>
                                    <tr>
                                        <th className="py-2 pr-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Producto</th>
                                        <th className="px-2 py-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Cantidad</th>
                                        <th className="px-2 py-2 text-xs font-medium tracking-wider text-gray-500 uppercase leading-4">Precio</th>
                                        <th className="py-2 pl-2 text-xs font-medium tracking-wider text-right text-gray-500 uppercase leading-4">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {relaciones.map((relacion) => (
                                        <tr key={relacion.id}>
                                            <td className="py-2 pr-2 whitespace-nowrap">{relacion.producto.producto} <br/> Marca: {relacion.producto.marca} <br/>Modelo: {relacion.producto.modelo} <br/>Año {relacion.producto.ano} <br/>Color: {relacion.producto.color} <br/>Chasis: {relacion.producto.chasis} <br/>Motor: {relacion.producto.motor} <br/>Cilindraje: {relacion.producto.cilindraje} <br/>Ramv: {relacion.producto.ramv}
                                            </td>
                                            <td className="px-2 py-2 whitespace-nowrap">{relacion.cantidad}</td>
                                            <td className="px-2 py-2 whitespace-nowrap">  <MonedaComponent /> {relacion.precio}</td>
                                            <td className="py-2 pl-2 text-right whitespace-nowrap">  <MonedaComponent/> {relacion.precio * relacion.cantidad}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>}


            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Example Modal"
            >

                <div className="border-b p-2 pb-3 pt-0 mb-4">
                    <div className="flex justify-between items-center">
                        Cerrar Factura
                        <span onClick={closeModal} className='close-modal cursor-pointer px-3 py-1 rounded-full bg-gray-100 hover:bg-gray-200'>
                            <i className="fas fa-times text-gray-700"></i>
                        </span>
                    </div>

                    <form id='form_id' className="w-full">
                        <div className="flex flex-wrap -mx-3 mb-6">
                            <div className="w-full md:w-1/1 px-3 mb-6 md:mb-0">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1">
                                    Total a Pagar
                                </label>
                                <input
                                    disabled
                                    value={total}
                                    className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                />
                            </div>

                            <div className="w-full md:w-1/1 px-3 mb-6 md:mb-0">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1">
                                    Metodo de Pago
                                </label>
                                <select
                                    onChange={(e) => setMetodoPago(e.target.value)}
                                    value={metodo_pago}
                                    className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                >
                                    <option value="0">Seleccione</option>
                                    <option value="1">Visa / Mastercard (Tarjeta de Debido o Credito)</option>
                                    <option value="2">Efectivo</option>
                                    <option value="3">Transferencia</option>
                                </select>
                            </div>
                        </div>

                        <div className="w-full md:w-1/1 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1">
                                Observaciones
                            </label>
                            <textarea
                                onChange={(e) => setObservaciones(e.target.value)}
                                value={observaciones}
                                className="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                            > </textarea>
                        </div>


                        <div className="mt-5 float-root mb-5">
                            <button onClick={cerrarFactura} className='bg-green-500 hover:bg-green-800 text-white font-bold py-2 px-4 rounded float-right mr-5'> Generar Factura </button>
                            <span onClick={closeModal} className='close-modal cursor-pointer bg-red-200 hover:bg-red-500 text-red-900 font-bold py-2 px-4 rounded float-right mr-5'>
                                Cerrar
                            </span>
                        </div>
                    </form>
                </div>
            </Modal>
        </Fragment>
    );
}

export default CreateFacturaComponent;