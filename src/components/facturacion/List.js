import React, { useEffect, useState, Fragment  } from 'react'
import { Link } from 'react-router-dom'
import clienteAxios from '../../config/axios';
import { Container, Button, Card, Alert, InputGroup, Form, Badge } from 'react-bootstrap'
import DataTable from 'react-data-table-component'
import LoaderComponent from '../adicionales/Loader'

const ListFacturasComponent = () => {

    const [facturas, setFacturas] = useState([])
    const [pending, setPending] = useState(true)

    const getFacturas = async () => {
        const res = await clienteAxios.get('/facturas')
        setFacturas(res.data)
        setPending(false)
        console.log(res.data)
    }

    useEffect(() => {
      getFacturas()
    }, [])
    

    const columns = [
        {
          name: '# FACTURA',
          selector: (row) => row.numero_documento,
          width: '120px',
        },
        {
          name: 'Cliente',
          selector: (row) => row.cliente.nombres + ' ' + row.cliente.apellidos,
        },
        {
          name: 'Total',
          selector: (row) => row.total,
        },
        {
          name: 'Estado',
          button: true,
          cell: (row) =>
            (row.estadoFactura === 1 && <Badge bg="success">Pagada</Badge>) || (
              <Badge bg="danger">Pendiente de Pago</Badge>
            ),
        },
        {
          name: '',
          button: true,
          cell: (row) => (
            <Fragment>
              <Button
                as={Link}
                to={`/factura/${row.id}`}
                variant="info"
                className="mr-2 text-white"
              >
                <i className="fas fa-eye"></i>
              </Button>
              
            </Fragment>
          ),
        },
      ]
    
      const paginationComponentOptions = {
        rowsPerPageText: 'Filas por página',
        rangeSeparatorText: 'de',
        selectAllRowsItem: true,
        selectAllRowsItemText: 'Todos',
      }

      const buscar = async (e) => {
        setPending(true)
        const res = await clienteAxios.get('/facturas/buscar/'+e)
        setFacturas(res.data.data)
        setPending(false)
      }

    return ( 
        <Container fluid="sm">
        <Card className="text-left">
          <Card.Header>Facturas</Card.Header>
          <Card.Body className="float-root">
            <Button
              as={Link}
              to={'/factura/create'}
              variant="success"
              className="float-right mb-2"
            >
              <i className="fa fa-plus-circle" /> Nueva Factura
            </Button>
            <InputGroup className="mb-3">
              <InputGroup.Text id="basic-addon1">
                <i className="fas fa-search"></i>
              </InputGroup.Text>
              <Form.Control
                placeholder="Buscador (Puede buscar Numero de Factura!)"
                aria-label="Buscador (Puede buscar Numero de Factura!)"
                aria-describedby="basic-addon1"
                onKeyPress={() => buscar()}
                onChange={(e) => buscar(e.target.value)}
              />
            </InputGroup>
            <DataTable
              columns={columns}
              data={facturas}
              responsive="true"
              progressPending={pending}
              progressComponent={<LoaderComponent />}
              pagination
              paginationComponentOptions={paginationComponentOptions}
            />
          </Card.Body>
        </Card>
      </Container>
     );
}
 
export default ListFacturasComponent;