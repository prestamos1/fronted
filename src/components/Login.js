import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import clienteAxios from '../config/axios';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2'

const LoginComponent = () => {

    const [usuario, setUsuario] = useState('')
    const [password, setPassword] = useState('')
    const navigate = useNavigate()

    const iniciarSesion = async (e) => {
        e.preventDefault();
        await clienteAxios.post('/login', {
            email: usuario,
            password: password
        }).then((res) => {
            console.log(res.data)
            localStorage.setItem('token', res.data.access_token)
            localStorage.setItem('usuario', res.data.user)
            Swal.fire({
                title: 'Sesion Iniciada!',
                text: 'Has iniciado sesion con exito!',
                icon: 'success',
                confirmButtonText: 'Cerrar'
            })
            navigate('/')
        }).catch(function (error) {
            if (error.response.status === 401)
            {
                Swal.fire({
                    title: 'Error!',
                    text: 'Datos erroneos!',
                    icon: 'error',
                    confirmButtonText: 'Cerrar'
                })
            }
        })
    }

    return (
        <Container>
            <Row >
                <Col md="4">
                </Col>
                <Col md="4" className="mt-5">
                    <form onSubmit={iniciarSesion}>
                        <Card >
                            <Card.Header>Login</Card.Header>
                            <Card.Body>
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Usuario</Form.Label>
                                    <Form.Control type="text" value={usuario} onChange={(e) => setUsuario(e.target.value)} />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Clave</Form.Label>
                                    <Form.Control type="text" value={password} onChange={(e) => setPassword(e.target.value)} />
                                </Form.Group>
                            </Card.Body>
                            <Card.Footer className="text-right">
                                <Button variant="success" type="submit"> <i className="fa fa-user" /> Iniciar Sesion</Button>
                            </Card.Footer>
                        </Card>
                    </form>
                </Col>
                <Col md="4">
                </Col>
            </Row>
        </Container>
    );
}

export default LoginComponent;