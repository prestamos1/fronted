import React, { Fragment } from 'react'
import { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import clienteAxios from '../../config/axios'
import { Container, Row, Col, Button, Form, Card, Alert } from 'react-bootstrap'
import Swal from 'sweetalert2'

const EditProductoComponent = () => {
  const [Marca, setMarca] = useState('')
  const [Ano, setAno] = useState('')
  const [Modelo, setModelo] = useState('')
  const [Chasis, setChasis] = useState('')
  const [Cilindraje, setCilindraje] = useState('')
  const [Motor, setMotor] = useState('')
  const [Color, setColor] = useState('')
  const [Ramv, setRamv] = useState('')
  const [estadoProducto, setEstadoProducto] = useState('')
  const [Proveedor, setProveedor] = useState('')
  const [NumeroPlaca, setNumeroPlaca] = useState('')
  const { id } = useParams()
  const navigate = useNavigate()

  const getProducto = async () => {
    const res = await clienteAxios.get('/producto/' + id)
    setMarca(res.data.marca)
    setAno(res.data.ano)
    setModelo(res.data.modelo)
    setChasis(res.data.chasis)
    setCilindraje(res.data.cilindraje)
    setMotor(res.data.motor)
    setColor(res.data.color)
    setRamv(res.data.ramv)
    setEstadoProducto(res.data.estadoProducto)
    setProveedor(res.data.proveedor)
    setNumeroPlaca(res.data.numero_placa)
  }

  useEffect(() => {
    getProducto()
  }, [])

  const update = (e) => {
    e.preventDefault()
    clienteAxios.post('/producto', {
      marca: Marca,
      modelo: Modelo,
      chasis: Chasis,
      motor: Motor,
      color: Color,
      ramv: Ramv,
      ano: Ano,
      cilindraje: Cilindraje,
      id: id,
      estadoProducto: estadoProducto,
      numero_placa: NumeroPlaca,
      proveedor: Proveedor
    })
    
    Swal.fire({
      title: 'Actualizacion Exitoso!',
      text: 'Has actualizado coreectamente el producto!',
      icon: 'success',
      confirmButtonText: 'Cerrar'
  })
    navigate('/productos')
  }
  return (
    <Fragment>
      <Container fluid="sm">
        <Card className="text-left">
          <Card.Header>Editar Producto</Card.Header>
          <form onSubmit={update}>
            <Card.Body>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Marca</Form.Label>
                    <Form.Control
                      required
                      value={Marca}
                      onChange={(e) => setMarca(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Modelo</Form.Label>
                    <Form.Control
                      required
                      value={Modelo}
                      onChange={(e) => setModelo(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Chasis</Form.Label>
                    <Form.Control
                      required
                      value={Chasis}
                      onChange={(e) => setChasis(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Motor</Form.Label>
                    <Form.Control
                      required
                      value={Motor}
                      onChange={(e) => setMotor(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Color</Form.Label>
                    <Form.Control
                      required
                      value={Color}
                      onChange={(e) => setColor(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Año</Form.Label>
                    <Form.Control
                      required
                      value={Ano}
                      onChange={(e) => setAno(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Proveedor</Form.Label>
                    <Form.Control
                      required
                      value={Proveedor}
                      onChange={(e) => setProveedor(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Numero de Placa</Form.Label>
                    <Form.Control
                      required
                      value={NumeroPlaca}
                      onChange={(e) => setNumeroPlaca(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Cilindraje</Form.Label>
                    <Form.Control
                      required
                      value={Cilindraje}
                      onChange={(e) => setCilindraje(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Ramv</Form.Label>
                    <Form.Control
                      required
                      value={Ramv}
                      onChange={(e) => setRamv(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
            </Card.Body>
            <Card.Footer className="text-right">
              <Button variant="success" type={'submit'}>
                {' '}
                <i className="fa fa-save" /> Guardar Producto
              </Button>
            </Card.Footer>
          </form>
        </Card>
      </Container>
    </Fragment>
  )
}

export default EditProductoComponent
