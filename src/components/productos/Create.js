import React, { Fragment } from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import clienteAxios from '../../config/axios'
import { Container, Row, Col, Button, Form, Card, Alert } from 'react-bootstrap'
import Swal from 'sweetalert2'
import axios from 'axios'

const CreateProductoComponent = () => {
  const [Marca, setMarca] = useState('')
  const [Ano, setAno] = useState('')
  const [Modelo, setModelo] = useState('')
  const [Chasis, setChasis] = useState('')
  const [Cilindraje, setCilindraje] = useState('')
  const [Motor, setMotor] = useState('')
  const [Color, setColor] = useState('')
  const [Ramv, setRamv] = useState('')
  const [Proveedor, setProveedor] = useState('')
  const [NumeroPlaca, setNumeroPlaca] = useState('')
  const [Precio, setPrecio] = useState('')
  const navigate = useNavigate()

  const store = async (e) => {
    e.preventDefault()
    const res = await clienteAxios.post('/productos', {
      marca: Marca,
      modelo: Modelo,
      chasis: Chasis,
      motor: Motor,
      color: Color,
      ramv: Ramv,
      ano: Ano,
      cilindraje: Cilindraje,
      numero_placa: NumeroPlaca,
      proveedor: Proveedor,
      precio: Precio,
    })

    console.log(res.data)

    const data2 = JSON.stringify({
      activo: true,
      codigo: res.data.data.idUnicoFacturacion,
      codigoAuxiliar: res.data.data.idUnicoFacturacion,
      codigoUnidad: '',
      descripcion: res.data.nombre,
      descuento: 0,
      idImpuestoIva: '96f18727-e8fd-4d51-8623-37d274edd046', //iva 12%
      nombre: res.data.nombre,
      numeroRuc: '1309714283001',
      permitirDescuento: false,
      permitirEdicionNombre: true,
      permitirEdicionPrecioVenta: true,
      precioVenta1: res.data.data.precio,
    })

    const config = {
      method: 'post',
      url: 'https://api-sbox.veronica.ec/api/v1.0/productos',
      headers: {
        accept: '*/*',
        'X-API-KEY': 'QBDAL5rLCvvn3csIKXoH',
        'Content-Type': 'application/json',
      },
      data: data2,
    }

    await axios(config)
      .then(function (response) {
        const id = response.data.result.id
        clienteAxios
          .get('/idApiFacturacion/' + res.data.data.id + '/' + id)
          .then((data) => {
            if(data.status == 200){
              Swal.fire({
                title: 'Registro Exitoso!',
                text: 'Has registrado coreectamente el producto!',
                icon: 'success',
                confirmButtonText: 'Cerrar',
              })
            }
          })
      })
      .catch(function (error) {
        console.log(error)
      })

    navigate('/productos')
  }
  return (
    <Fragment>
      <Container fluid="sm">
        <Card className="text-left">
          <Card.Header>Crear Producto</Card.Header>
          <form onSubmit={store}>
            <Card.Body>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Marca</Form.Label>
                    <Form.Control
                      required
                      value={Marca}
                      onChange={(e) => setMarca(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Modelo</Form.Label>
                    <Form.Control
                      required
                      value={Modelo}
                      onChange={(e) => setModelo(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Chasis</Form.Label>
                    <Form.Control
                      required
                      value={Chasis}
                      onChange={(e) => setChasis(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Motor</Form.Label>
                    <Form.Control
                      required
                      value={Motor}
                      onChange={(e) => setMotor(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Color</Form.Label>
                    <Form.Control
                      required
                      value={Color}
                      onChange={(e) => setColor(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Año</Form.Label>
                    <Form.Control
                      required
                      value={Ano}
                      onChange={(e) => setAno(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Proveedor</Form.Label>
                    <Form.Control
                      required
                      value={Proveedor}
                      onChange={(e) => setProveedor(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Numero de Placa</Form.Label>
                    <Form.Control
                      required
                      value={NumeroPlaca}
                      onChange={(e) => setNumeroPlaca(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Cilindraje</Form.Label>
                    <Form.Control
                      required
                      value={Cilindraje}
                      onChange={(e) => setCilindraje(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Ramv</Form.Label>
                    <Form.Control
                      required
                      value={Ramv}
                      onChange={(e) => setRamv(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row md="2" sm="1" xs="1">
                <Col>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Precio</Form.Label>
                    <Form.Control
                      required
                      value={Precio}
                      onChange={(e) => setPrecio(e.target.value)}
                      type="text"
                    />
                  </Form.Group>
                </Col>
              </Row>
            </Card.Body>
            <Card.Footer className="text-right">
              <Button variant="success" type={'submit'}>
                {' '}
                <i className="fa fa-save" /> Guardar Producto
              </Button>
            </Card.Footer>
          </form>
        </Card>
      </Container>
    </Fragment>
  )
}

export default CreateProductoComponent
