import React, { useEffect, useState, Fragment } from 'react'
import { Link } from 'react-router-dom'
import clienteAxios from '../../config/axios'
import { Container, Button, Card, Alert, InputGroup, Form, Badge } from 'react-bootstrap'
import DataTable from 'react-data-table-component'
import LoaderComponent from '../adicionales/Loader'

const ListProductosComponent = () => {
  const [productos, setProducto] = useState([])
  const [Buscar, setBuscar] = useState()
  const [eliminar, setEliminar] = useState('')
  const [pending, setPending] = React.useState(true)

  const getProductos = async () => {
    const res = await clienteAxios.get('/productos')
    setProducto(res.data)
    setPending(false)
  }

  const deleteProducto = async (id) => {
    const res = await clienteAxios.get('/productod/' + id)

    if (res.data.status === true) {
      getProductos()
      setEliminar(true)

      setTimeout(() => {
        setEliminar(false)
      }, 5000)
    }
  }

  useEffect(() => {
    getProductos()
  }, [])

  const columns = [
    {
      name: '#',
      selector: (row) => row.id,
      width: '40px',
    },
    {
      name: 'Nombre',
      selector: (row) =>
        row.marca + ' ' + row.modelo + ' ' + row.color + ' ' + row.chasis,
    },
    {
      name: 'Sincronizado',
      button: true,
      cell: row => (
        row.idApiFacturacion != null
        ?
        <i class="fa fa-check-circle text-success"></i> : <i class="fa fa-check-circle text-danger"></i> 
    )
    },
    {
      name: '',
      button: true,
      cell: (row) => (
        <Fragment>
          <Button
            as={Link}
            to={`/producto/edit/${row.id}`}
            variant="success"
            className="mr-2"
          >
            <i className="fas fa-edit"></i>
          </Button>
          <Button onClick={() => deleteProducto(row.id)} variant="danger">
            <i className="fas fa-trash"></i>
          </Button>
        </Fragment>
      ),
    },
  ]

  const paginationComponentOptions = {
    rowsPerPageText: 'Filas por página',
    rangeSeparatorText: 'de',
    selectAllRowsItem: true,
    selectAllRowsItemText: 'Todos',
  }

  const myNewTheme = {
    rows: {
      fontSize: '25px',
    },
  }

  const buscar = async (e) => {
    setPending(true)
    const res = await clienteAxios.get('/productos/buscar/'+e)
    setProducto(res.data.data)
    setPending(false)
  }

  return (
    <Fragment>
      <Container fluid="sm">
        <Card className="text-left">
          <Card.Header>Productos</Card.Header>
          <Card.Body className="float-root">
            <Button
              as={Link}
              to={'/producto/create'}
              variant="success"
              className="float-right mb-1"
            >
              <i className="fa fa-plus-circle" /> Agregar Producto
            </Button>
            <InputGroup className="mb-3">
              <InputGroup.Text id="basic-addon1"><i className="fas fa-search"></i></InputGroup.Text>
              <Form.Control
                placeholder="Buscador (Puede buscar por Chasis, Ramv, Numero de Placa!)"
                aria-label="Buscador (Puede buscar por Chasis, Ramv, Numero de Placa!)"
                aria-describedby="basic-addon1"
                onKeyPress={() => buscar()}
                onChange={(e) => buscar(e.target.value)}
              />
            </InputGroup>
            {eliminar && (
              <Alert variant={'success'}>Producto Eliminado con exito!</Alert>
            )}
            <DataTable
              columns={columns}
              data={productos}
              responsive="true"
              progressPending={pending}
              progressComponent={<LoaderComponent />}
              pagination
              paginationComponentOptions={paginationComponentOptions}
              customTheme={myNewTheme}
            />
          </Card.Body>
        </Card>
      </Container>
    </Fragment>
  )
}

export default ListProductosComponent
