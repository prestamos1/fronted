import React, { useEffect, useState } from 'react';
import clienteAxios from '../../config/axios'

const MenuGeneralComponent = () => {

    const [TotalAbono, setTotalAbono] = useState()
    const [TotalClientes, setTotalClientes] = useState()
    const [TotalFacturas, setTotalFacturas] = useState()
    const [TotalCreditos, setTotalCreditos] = useState()

    const getData = async() => {
        const data = await clienteAxios.get('/data_index')
        console.log(data.data)
        setTotalAbono(data.data.total_abono)
        setTotalCreditos(data.data.total_creditos)
        setTotalClientes(data.data.total_clientes)
        setTotalFacturas(data.data.total_facturas)
    }

    useEffect(() => {
        getData()
    },[])


    return ( 
        <div className="flex flex-1 flex-col md:flex-row lg:flex-row mx-2">
            <div className="shadow-lg bg-red-vibrant border-l-8 hover:bg-red-vibrant-dark border-red-vibrant-dark mb-2 p-2 md:w-1/4 mx-2">
                <div className="p-4 flex flex-col">
                    <span className="no-underline text-white text-2xl">
                        {TotalCreditos}
                    </span>
                    <span className="no-underline text-white text-lg">
                        Total Creditos
                    </span>
                </div>
            </div>

            <div className="shadow bg-info border-l-8 hover:bg-info-dark border-info-dark mb-2 p-2 md:w-1/4 mx-2">
                <div className="p-4 flex flex-col">
                    <span className="no-underline text-white text-2xl">
                        $. {TotalAbono}
                    </span>
                    <span className="no-underline text-white text-lg">
                        Total Ingresos Hoy
                    </span>
                </div>
            </div>

            <div className="shadow bg-warning border-l-8 hover:bg-warning-dark border-warning-dark mb-2 p-2 md:w-1/4 mx-2">
                <div className="p-4 flex flex-col">
                    <span className="no-underline text-white text-2xl">
                        {TotalClientes}
                    </span>
                    <span className="no-underline text-white text-lg">
                        Total Clientes
                    </span>
                </div>
            </div>

            <div className="shadow bg-success border-l-8 hover:bg-success-dark border-success-dark mb-2 p-2 md:w-1/4 mx-2">
                <div className="p-4 flex flex-col">
                    <span className="no-underline text-white text-2xl">
                        {TotalFacturas}
                    </span>
                    <span className="no-underline text-white text-lg">
                        Total Facturas
                    </span>
                </div>
            </div>
        </div>
     );
}
 
export default MenuGeneralComponent;