import React, { useState } from 'react';
import { Link } from 'react-router-dom'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

const HeaderComponent = () => {

    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="#home">PrestaSIS</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link as={Link} to={'/'}>Inicio</Nav.Link>
                        <NavDropdown title="Configuraciones" id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to={'/empresa'}>Empresa</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to={'/usuarios'}>  Usuarios </NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Facturacion" id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to={'/clientes'}>Clientes</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to={'/productos'}>  Productos </NavDropdown.Item>
                            <NavDropdown.Item as={Link} to={'/facturas'}>  Facturas </NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Creditos" id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to={'/creditos'}>Creditos</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to={'/simulador-creditos'}>  Simulador de Credito </NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Reportes" id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to={'/clientes'}>Cuotas a Pagar Hoy</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to={'/clientes'}>Reporte de Moras</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to={'/productos'}>  Filtrar Cuotas Por Fecha </NavDropdown.Item>
                            <NavDropdown.Item as={Link} to={'/productos'}>  Filtrar Cuotas Por Cliente </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Container>
            </Navbar>
    )
}

export default HeaderComponent