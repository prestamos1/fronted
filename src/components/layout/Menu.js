import React from 'react';
import { Link } from 'react-router-dom'


const MenuComponent = () => {
    return (
        <aside id="sidebar" className="bg-side-nav w-1/2 md:w-1/6 lg:w-1/6 border-r border-side-nav hidden md:block lg:block">
            <ul className="list-reset flex flex-col">
                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">
                        <i className="fas fa-home float-left mx-2"></i>
                        Inicio
                    </Link>
                </li>
                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/empresa" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">

                        <i className="fa fa-building float-left mx-2"></i>
                        Empresa
                    </Link>
                </li>
                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/usuarios" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">
                        <i className="fas fa-users float-left mx-2"></i>
                        Usuarios
                    </Link>
                </li>
                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/clientes" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">
                        <i className="fas fa-user float-left mx-2"></i>
                        Clientes
                    </Link>
                </li>
                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/productos" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">
                        <i className="fas fa-box float-left mx-2"></i>
                        Productos
                    </Link>
                </li>
                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/facturas" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">
                        <i className="fas fa-book float-left mx-2"></i>
                        Facturas
                    </Link>
                </li>
                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/simulador-creditos" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">
                        <i className="fas fa-calculator float-left mx-2"></i>
                        Simulador de Credito
                    </Link>
                </li>
                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/creditos" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">
                        <i className="fas fa-wallet float-left mx-2"></i>
                        Creditos
                    </Link>
                </li>

                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/cuotas/hoy" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">
                        <i className="fas fa-wallet float-left mx-2"></i>
                        Cuotas a Pagar Hoy
                    </Link>
                </li>

                <li className=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                    <Link to="/cuotas/hoy" className="font-sans font-hairline hover:font-normal text-sm text-nav-item no-underline">
                        <i className="fas fa-wallet float-left mx-2"></i>
                        Filtrar Cuotas Por Fecha
                    </Link>
                </li>
            </ul>

        </aside> 
        );
}

export default MenuComponent;