// Layouts
import React from 'react';

import './assets/menu.css'
import './index.css'
// import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import HeaderComponent from "./components/layout/Header";
import MenuComponent from "./components/layout/Menu";
import MenuGeneralComponent from './components/layout/MenuGeneral';
import { Routes, Route } from 'react-router-dom';
import EditEmpresaComponent from './components/empresa/Edit';
import ListUsuariosComponent from './components/usuarios/List';
import CreateUsuarioComponent from './components/usuarios/Create';
import EditUserComponent from './components/usuarios/Edit';
import ListClientesComponent from './components/clientes/List';
import CreateClienteComponent from './components/clientes/Create';
import EditCLienteComponent from './components/clientes/Edit';
import ListProductosComponent from './components/productos/List';
import CreateProductoComponent from './components/productos/Create';
import EditProductoComponent from './components/productos/Edit';
import ListFacturasComponent from './components/facturacion/List';
import ViewFacturaComponent from './components/facturacion/View';
import CreateFacturaComponent from './components/facturacion/Create';
import SimuladorComponent from './components/credito/Simulador';
import ListCreditosComponent from './components/credito/List';
import CreateCreditoComponent from './components/credito/Create';
import ViewClienteComponent from './components/clientes/View';
import ViewCuotaComponent from './components/credito/ViewCuota';
import ViewCreditoComponent from './components/credito/ViewCredito';
import ViewCuotasHoyComponent from './components/credito/CuotasHoy';
import FilterCuotasComponent from './components/credito/FiltrarCuotas';
import FilterCuotasClienteComponent from './components/credito/FiltrarCuotasCliente';
import LoginComponent from './components/Login';


function App() {
  return (
    <div className="mx-auto bg-grey-400">
      <div className="min-h-screen flex flex-col">
        <HeaderComponent />
        <div className="flex flex-1">

          <main className="bg-white-300 flex-1 p-3 overflow-hidden">
              <Routes>
              <Route path="/" element={<MenuGeneralComponent />}></Route>
              <Route path="/login" element={<LoginComponent />}></Route>
              <Route path="/empresa" element={<EditEmpresaComponent />}></Route>
              <Route path="/usuarios" element={<ListUsuariosComponent />}></Route>
              <Route path="/usuario/create" element={<CreateUsuarioComponent />}></Route>
              <Route path="/usuario/edit/:id" element={<EditUserComponent />}></Route>
              <Route path="/clientes" element={<ListClientesComponent />}></Route>
              <Route path="/cliente/:id" element={<ViewClienteComponent />}></Route>
              <Route path="/cliente/create" element={<CreateClienteComponent />}></Route>
              <Route path="/cliente/edit/:id" element={<EditCLienteComponent />}></Route>
              <Route path="/productos" element={<ListProductosComponent />}></Route>
              <Route path="/producto/create" element={<CreateProductoComponent />}></Route>
              <Route path="/producto/edit/:id" element={<EditProductoComponent />}></Route>
              <Route path="/facturas" element={<ListFacturasComponent />}></Route>
              <Route path="/factura/:id" element={<ViewFacturaComponent />}></Route>
              <Route path="/factura/create" element={<CreateFacturaComponent />}></Route>
              <Route path="/simulador-creditos" element={<SimuladorComponent />}></Route>
              <Route path="/creditos" element={<ListCreditosComponent />}></Route>
              <Route path="/credito/create" element={<CreateCreditoComponent />}></Route>
              <Route path="/cuota/pagar/:id" element={<ViewCuotaComponent />}></Route>
              <Route path="/credito/:id" element={<ViewCreditoComponent />}></Route>
              <Route path="/cuotas-hoy" element={<ViewCuotasHoyComponent />}></Route>
              <Route path="/buscar" element={<FilterCuotasComponent />}></Route>
              <Route path="/buscar/cliente" element={<FilterCuotasClienteComponent />}></Route>
              </Routes>
          </main>
        </div>
      </div>
      </div>
  );
}

export default App;

